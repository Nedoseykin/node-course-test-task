const isExists = v => v !== undefined && v !== null;

const isNotEmptyArray = v => Array.isArray(v) && v.length > 0;

const findArrayItemIndex = (arr, filter) => {
  if (!isNotEmptyArray(arr)) return -1;
  const fields = !!filter
    ? Object.keys(filter)
    : [];
  if (fields.length === 0) return arr[ 0 ];
  for (let i = 0; i < arr.length; i++) {
    let test = true;
    let j = 0;
    while (j < fields.length && test) {
      test = (fields[ j ] in arr[ i ])
        && (arr[ i ][ fields[ j ] ] === filter[ fields[ j ] ]);
      ++j;
    }
    if (test) return i;
  }
  return -1;
};

const findArrayItem = (arr, filter) => {
  const i = findArrayItemIndex(arr, filter);
  return i === -1 ? null : arr[ i ];
};

export {
  isExists,
  isNotEmptyArray,
  findArrayItemIndex,
  findArrayItem,
};
