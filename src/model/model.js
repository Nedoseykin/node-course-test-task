import { thenStrategy } from './strategies/thenStrategy';
import { asyncStrategy } from './strategies/asyncStrategy';
import { generatorStrategy } from './strategies/generatorStrategy';

export const MODEL = {
  case1: {
    title: 'Promise.then + functions',
    action: thenStrategy,
  },

  case2: {
    title: 'Async/await + arrows',
    action: asyncStrategy,
  },

  case3: {
    title: 'Generators',
    action: generatorStrategy,
  }
};
