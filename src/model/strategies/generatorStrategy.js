import { setMainValue, showModal } from '../../index';
import { loadData } from '../../networking/request';

async function* requestSequence() {
  while (Array.isArray(window.urlsArray) && window.urlsArray.length) {
    const url = window.urlsArray.shift();
    const loadedData = await loadData({url});
    if (typeof loadedData === 'string') {
      window.wordsArray.push(loadedData);
      yield window.wordsArray.join(' ');
    } else if (Array.isArray(loadedData) && loadedData.length) {
      window.urlsArray = loadedData.concat(window.urlsArray);
    }
  }
}

export const generatorStrategy = async () => {
  for await (const value of requestSequence() ) {
    setMainValue(value);
  }
  setTimeout(() => {showModal(false)}, 1000);
};
