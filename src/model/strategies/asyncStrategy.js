import { loadData } from '../../networking/request';
import { setMainValue, showModal } from '../../index';

export const asyncStrategy = async () => {
  while (Array.isArray(window.urlsArray) && window.urlsArray.length) {
    const url = window.urlsArray.shift();
    const loadedData = await loadData({url});
    if (typeof loadedData === 'string') {
      window.wordsArray.push(loadedData);
      setMainValue(window.wordsArray.join(' '));
    } else if (Array.isArray(loadedData) && loadedData.length) {
      window.urlsArray = loadedData.concat(window.urlsArray);
    }
  }
  setTimeout(() => {showModal(false)}, 1000);
};
