import { loadData } from '../../networking/request';
import { setMainValue, showModal } from '../../index';

export function thenStrategy() {
  if (Array.isArray(window.urlsArray) && window.urlsArray.length) {
    const url = window.urlsArray.shift();
    loadData({
      url,
      cbError: function() {
        thenStrategy();
      },
      cbSuccess: function(loadedData) {
        if (typeof loadedData === 'string') {
          window.wordsArray.push(loadedData);
          setMainValue(window.wordsArray.join(' '));
        } else if (Array.isArray(loadedData) && loadedData.length) {
          window.urlsArray = loadedData.concat(window.urlsArray);
        }
        thenStrategy();
      }
    });
  } else {
    setTimeout(function() {showModal(false);}, 1000);
  }
}
