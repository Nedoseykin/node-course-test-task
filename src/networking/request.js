import { API_URL } from './networking-consts';

export function loadData(options) {
  if (!options) return;
  const {
    url,
    cbError,
    cbSuccess,
  } = options;

  const request = new Request(
    `${API_URL}/${url}`,
    {
      method: 'GET',
      headers: {
        'Content-Type': 'text/plain',
      },
      // mode: 'cors',
    }
  );

  return fetch(request)
    .then(function(response) {
      const {status} = response;
      if (!response.ok) {
        console.error('HTTP error, status: ', status);
        !!cbError && cbError({status});
        return;
      }
      const { headers } = response;
      const contentType = headers.get('Content-Type');
      if (contentType.includes('text/plain')) {
        return response.text();
      }
      !!cbError && cbError({status, message: 'Content type is not a text/plain'});
    })
    .then(function(text) {
      let loadedData;
      try {
        loadedData = JSON.parse(text);
      } catch (e) {
        loadedData = text;
      }
      !!cbSuccess && cbSuccess(loadedData);
      return loadedData;
    })
    .catch(function() {
      !!cbError && cbError();
      return null;
    });
}
