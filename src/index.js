import "core-js";
import 'whatwg-fetch';
import 'raf/polyfill';
import './style/style.scss';
import { MODEL } from './model/model';

const defaultUrls = ['root.txt'];
window.urlsArray = [];
window.wordsArray = [];

document.addEventListener('DOMContentLoaded', main, false);

function main() {
  const toolbarBtns = document.querySelectorAll('.toolbar-root .toolbar--button');
  Array.prototype.forEach.call(toolbarBtns, btn => {
    btn.addEventListener('click', handleTabClick(btn.id), false);
  })
}

function handleTabClick(btnId) {
  return function() {
    const toolbarBtns = document.querySelectorAll('.toolbar-root .toolbar--button');
    Array.prototype.forEach.call(toolbarBtns, btn => {
      const btnClass = `toolbar--button${ btn.id === btnId ? ' toolbar--button__active' : '' }`;
      btn.setAttribute('class', btnClass);
    });
    calculate(btnId);
  }
}

function calculate(btnId) {
  const {title, action} = MODEL[btnId];
  if (action) {
    showModal(true);
    window.wordsArray = [];
    window.urlsArray = [...defaultUrls];
    setMainValue('');
    action();
  } else {
    setMainValue('Action is not implemented');
  }
}

export function setMainValue(value) {
  const main = document.querySelector('main');
  if (main) main.innerHTML = value;
}

export function showModal(show) {
  const modal = document.getElementById('modalRoot');
  const modalClass = show
    ? 'modal--root modal--root__active'
    : 'modal--root';
  modal.setAttribute('class', modalClass);
}
