const path = require('path');

const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

const PATH_CONFIG = path.resolve(__dirname);
const PATH_ROOT = path.resolve(PATH_CONFIG, '..');
const PATH_SRC = path.resolve(PATH_ROOT, 'src');
const PATH_DIST = path.resolve(PATH_ROOT, 'dist');
const PATH_TEMPLATES = path.resolve(PATH_ROOT, 'templates');

const DEVELOPMENT = 'development';
const PRODUCTION = 'production';

const REG_JS = /\.(js)$/i;
const REG_NODE_MODULES = /(node_modules|bower_components)/i;
const REG_CSS = /\.s?[ac]ss$/i;
const REG_IMAGES = /\.(jpe?g|png|svg|gif)$/i;
const REG_FONTS = /\.(woff|woff2|eot|ttf|otf)$/i;
const REG_CSV = /\.(csv|tsv)$/i;
const REG_XML = /\.xml$/i;

const HTML_TITLE = 'Test task for node js educational course';

const config = function (mode) {
  console.log('config: mode: ', mode);

  const styleLoader = mode !== PRODUCTION
    ? {
        loader: 'style-loader',
        options: { sourceMap: true },
      }
    : MiniCssExtractPlugin.loader;

  const cssLoader = mode !== PRODUCTION
    ? {
        loader: 'css-loader',
        options: { sourceMap: true },
      }
    : 'css-loader';

  const postCssLoader = {
    loader: 'postcss-loader',
    options: {
      config: {
        path: PATH_CONFIG,
      },
      sourceMap: mode !== PRODUCTION,
    },
  };

  const sassLoader = {
        loader: 'sass-loader',
        options: {
          sourceMap: mode !== PRODUCTION,
          data: '$env: ' + mode + ';'
        },
      };

  const fileLoader = 'file-loader';

  const imageWebpackLoader = {
    loader: 'image-webpack-loader',
    options: {
      mozjpeg: {
        progressive: true,
        quality: 65
      },
      // optipng.enabled: false will disable optipng
      optipng: {
        enabled: false,
      },
      pngquant: {
        quality: '65-90',
        speed: 4
      },
      gifsicle: {
        interlaced: false,
      },
      // the webp option will enable WEBP
      // webp: {
      //   quality: 75
      // }
    }
  };

  const devtool = mode !== PRODUCTION
    ? 'inline-source-map'
    : false;

  return {
    mode: mode,
    entry: {
      app: path.resolve(PATH_SRC, 'index.js'),
    },
    devtool,
    devServer: {
      contentBase: PATH_DIST,
      publicPath: '',
      compress: true,
      port: 3000,
      clientLogLevel: 'silent',
      stats: 'errors-only',
      // hot: true,
    },
    resolve: { extensions: ["*", ".js", ".jsx"] },
    output: {
      filename: '[name].[hash].bundle.js',
      path: PATH_DIST,
      publicPath: '',
    },
    module: {
      rules: [
        {
          test: REG_JS,
          exclude: REG_NODE_MODULES,
          loader: 'babel-loader',
          options: {
            presets: [
              '@babel/preset-env',
            ],
            plugins: [
              '@babel/plugin-transform-runtime',
              ['@babel/plugin-transform-regenerator', {
                'asyncGenerators': false,
                'generators': false,
                'async': false
              }],
              [ '@babel/plugin-proposal-decorators', { legacy: true } ],
              '@babel/plugin-proposal-class-properties',
            ],
          }
        },
        {
          test: REG_CSS,
          use: [
            styleLoader,
            cssLoader,
            postCssLoader,
            sassLoader,
          ],
        },
        {
          test: REG_IMAGES,
          use: [
            fileLoader,
            imageWebpackLoader,
          ],
        },
        {
          test: REG_FONTS,
          use: [ fileLoader ],
        },
        {
          test: REG_CSV,
          use: [ 'csv-loader' ],
        },
        {
          test: REG_XML,
          use: [ 'xml-loader' ],
        },
      ],
    },
    plugins: [
      new MiniCssExtractPlugin({
        filename: '[name].[hash].css',
        chunkFilename: '[id].css',
      }),
      new CleanWebpackPlugin(),
      new HtmlWebpackPlugin({
        title: HTML_TITLE,
        template: path.resolve(PATH_TEMPLATES, 'index.html'),
        cache: true,
      })
    ],
  };
};

module.exports = (env, argv) => {
  // console.log('argv: ', argv);
  var mode = argv.mode === DEVELOPMENT ? DEVELOPMENT : PRODUCTION;

  return config(mode);
};
